import randomNormal from "random-normal";

export default function gendata(temperature, samplingRate, acquisitionTime) {
  // experimental parameters [visible to operator]
  const T = temperature; // temperature in units of K
  const S = samplingRate; // sampling rate of ADC in units of Hz
  const t_meas = acquisitionTime; // data acquisition time in units of s

  // intrinsic experimental parameters [hidden from operator]
  // resonator properties
  const m = 5.5e-15; // resonator mass in units of kg
  const f0 = 5.5e3; // resonance frequency in units of Hz
  const w0 = 2 * Math.PI * f0; // angular resonance frequency in units of rad / s
  const Q = 4.5e4; // mechanical quality factor, no units

  // other parameters
  const kB = 1.38e-23; // Boltzmann constant in units of J / K
  const h = 6.63e-34; // Plack constant in units of Js
  const G = 1e7; // conversion gain of the interferometer in units of V / m
  const s_n = 1e-6; // voltage noise PSD in units of V ^ 2 / Hz
  const s_f = (4 * kB * T * m * w0) / Q; // mean value of thermomechanical noise PSD(single - sided) in units of N ^ 2 / Hz
  const tau = Q / (Math.PI * f0);

  // initialize PSD arrays
  const df = 1 / t_meas; // frequency resolution
  const N = (t_meas * S) / 2; // number of point in PSD arrays
  const n = t_meas / tau; // number of uncorrelated variance estimations

  let freq = [];
  let S_v = [];
  for (let i = 0; i <= N; i++) {
    const f = df + (i * S) / (2 * N);
    const S_f = Math.abs(randomNormal({ mean: s_f, dev: s_f / Math.sqrt(n) }));
    const chi_squared =
      1 /
      m ** 2 /
      Math.pow(2 * Math.PI, 4) /
      ((f0 ** 2 - f ** 2) ** 2 + (f0 ** 2 * f ** 2) / Q ** 2);
    const S_x = S_f * chi_squared;

    const S_amp = Math.abs(randomNormal({ mean: s_n, dev: s_n / N }));

    freq.push(f);
    S_v.push(S_x * G ** 2 + S_amp);
  }
  return { freq, S_v };
}
