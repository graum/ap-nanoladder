import React from "react";
import Plot from "./plot";
import { saveAs } from "file-saver";

import generate_data from "./generate_data";

import "./css/experiment.css";

export default class Experiment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      temperature: 30,
      samplingRate: 1000000,
      acqusitionTime: 0.0025,
      data: { freq: [], S_v: [] }
    };

    this.save = this.save.bind(this);
    this.measure = this.measure.bind(this);
  }

  componentDidMount() {
    this.measure();
  }

  measure() {
    this.setState({
      data: generate_data(
        this.state.temperature,
        this.state.samplingRate,
        this.state.acqusitionTime
      )
    });
  }

  save() {
    const csv = ["% frequency (Hz),PSD (V^2/Hz)"]
      .concat(
        this.state.data.freq.map(
          (freq, index) => `${freq},${this.state.data.S_v[index]}`
        )
      )
      .join("\n");

    const blob = new Blob([csv], {
      type: "text/plain;charset=utf-8"
    });

    const filename = `PSD-T-${this.state.temperature}K-Srate-${this.state.samplingRate}Hz-tmeas-${this.state.acqusitionTime}.dat`;

    saveAs(blob, filename);
  }

  render() {
    return (
      <div id="experiment">
        <h1>AP Nanoladder Experiment</h1>
        <Plot data={this.state.data} />
        <div id="controls">
          <p className="options">
            <label>Temperature:</label>
            <input
              type="text"
              id="temp"
              name="temp"
              value={this.state.temperature}
              onChange={event =>
                this.setState({ temperature: event.target.value })
              }
            />
            <label>Kelvin</label>

            <label>Sampling Rate:</label>
            <input
              type="text"
              id="sampling"
              name="sampling"
              value={this.state.samplingRate}
              onChange={event =>
                this.setState({ samplingRate: event.target.value })
              }
            />
            <label>Hz</label>

            <label>Acquisition Time:</label>
            <input
              type="text"
              id="actime"
              name="actime"
              value={this.state.acqusitionTime}
              onChange={event =>
                this.setState({ acqusitionTime: event.target.value })
              }
            />
            <label>seconds</label>
          </p>
          <div className="buttons">
            <button type="button" id="plot" onClick={() => this.measure()}>
              Measure
            </button>
            <button onClick={() => this.save()}>Save Data</button>
          </div>
        </div>
      </div>
    );
  }
}
